#! /usr/bin/env python

from __future__ import print_function, division

import os
import numpy as np

from argparse import ArgumentParser

from astropy.coordinates import SkyCoord
from astropy import units as u

from flux_warp.catalogue_match import Catalogue, match, write_out
from flux_warp import CustomFormatter, get_data, _DEFAULT_MODEL_CATALOGUE 

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def main():
    """
    """

    _description = """
    Simple match of one catalogue to another. 
    Coordinates from the first catalogue are then stored as 'old_ra', 
    'old_dec', and the second catalogue's coordinates are considered the true 
    coordinates. The output format is suitable for fits_warp.py and flux_warp.
    """

    _epilog = """
    Many of the default key values assume a catalogue prepared using Aegean:
    https://github.com/PaulHancock/Aegean
    """

    _help = {"cat1": "The first catalogue - .vot or .fits format.",
             "cat2": "The second catalogue - .vot or .fits format.",
             "s": "Minimum angular separation required for match. [Default None]",
             "z": "Exclusion zone around sources. [Default 0 deg]",
             "r1": "RA key for catalogue 1. [Default 'ra']",
             "d1": "DEC key for catalogue 1. [Default 'dec']",
             "r2": "RA key for catalogue 2. [Default 'ra']",
             "d2": "DEC key for catalogue 2. [Default 'dec']",
             "o": "Output catalogue name.",
             "F": "Flux density column name for catalogue 1. [Default 'int_flux']",
             "E": "Flux density error column name for catalogue 1. "
                  "[Default 'err_int_flux']",
             "t": "Flux density threshold for including sources. [Default 0 Jy]",
             "n": "Maximum number of sources to include in the match. If more "
                  "sources are matched, a flux threshold is used if flux key "
                  "is supplied/found. [Default 1000]",
             "l": "Local RMS column name for catalogue 1. [Default 'local_rms']",
             "c": "Coordinates to centre match around (deg). [Default None]",
             "S": "Radius within which to match source (deg). [Default None]",
             "b1": "Beam major and minor axes for catalogue 1. [Default None]",
             "S1": "Source size keys for major and minor axes for catalogue 1. "
                   "[Default None]",
             "P1": "Source position angle key for catalogue 1. [Default None]",
             "b2": "Beam for catalogue 2. [Default None]",
             "S2": "Source size keys for catalogue 2. [Default None]",
             "ratio_flux": "Ratio of int_flux/peak_flux that defines compactness. "
                      "[Default 1.2]",
             "ratio_beam": "Ratio of a*b/psf_a*psf_b that defines compactness. [Default -1, not used.]",
             "bk1": "Column name for PSF for catalogue 1. [Default None]",
             "bk2": "Column name for PSF for catalogue 2. [Default None]",
             "f1": "Column names for integrated and peak flux densities in "
                   "catalogue 1. Used if they exist in table. " 
                   "[Default 'int_flux' 'peak_flux']",
             "f2": "Column names for integrated and peak flux densities in "
                   "catalogue 2. Used if they exist in table. " 
                   "[Default 'int_flux' 'peak_flux']",
             "cite": "Print citation information."
             }

    fmt = lambda prog: CustomFormatter(prog)
    ps = ArgumentParser(description=_description, epilog=_epilog, 
                        formatter_class=fmt)

    ps.add_argument("catalogue1", type=str, help=_help["cat1"])
    ps.add_argument("catalogue2", type=str, help=_help["cat2"])

    ps1 = ps.add_argument_group("cross-matching options/inputs")
    ps1.add_argument("-s", "--separation", type=float, default=None,
                     help=_help["s"])
    ps1.add_argument("-z", "--exclusion_zone", dest="zone", type=float,
                     default=0., help=_help["z"])
    ps1.add_argument("-o", "--outname", type=str, default=None, help=_help["o"])
    ps1.add_argument("-t", "--threshold", type=float, default=0., 
                     help=_help["t"])
    ps1.add_argument("-n", "--nmax", type=int, default=1000, help=_help["n"])
    ps1.add_argument("-c", "--coords", type=float, nargs=2, default=None,
                     help=_help["c"], metavar=("RA", "DEC"))
    ps1.add_argument("-S", "--radius", type=float, default=10.0,
                     help=_help["S"])
    ps1.add_argument("--cite", action="store_true", help=_help["cite"])

    ps2 = ps.add_argument_group("catalogue column names and inputs") 
    ps2.add_argument("-r", "--ra1", type=str, default="ra", help=_help["r1"])
    ps2.add_argument("-d", "--dec1", type=str, default="dec", help=_help["d1"])
    ps2.add_argument("-R", "--ra2", type=str, default="ra", help=_help["r2"])
    ps2.add_argument("-D", "--dec2", type=str, default="dec", help=_help["d2"])
    ps2.add_argument("-F", "--flux_key", type=str, default="int_flux", help=_help["F"])
    ps2.add_argument("-E", "--eflux_key", type=str, default="err_int_flux", 
                     help=_help["E"])
    ps2.add_argument("-l", "--localrms_key", type=str, default="local_rms",
                     help=_help["l"])
    ps2.add_argument("-b1", "--beam1", type=float, nargs=2, default=None,
                     help=_help["b1"], metavar=("BMAJ1", "BMIN1"))
    ps2.add_argument("-bk1", "--beam_key1", type=str, nargs=2, default=[None, None],
                     help=_help["bk1"], metavar=("BMAJ_KEY1", "BMIN_KEY1"))
    ps2.add_argument("-S1", "--size1", type=str, nargs=2, default=[None, None],
                     help=_help["S1"], metavar=("MAJ1", "MIN1"))
    ps2.add_argument("-P1", "--pa1", type=str, default=None, 
                     help=_help["P1"])
    ps2.add_argument("-b2", "--beam2", type=float, nargs=2, default=None,
                     help=_help["b2"], metavar=("BMAJ2", "BMAJ2"))
    ps2.add_argument("-bk2", "--beam_key2", type=str, nargs=2, default=None,
                     help=_help["bk2"], metavar=("BMAJ_KEY2", "BMIN_KEY2"))
    ps2.add_argument("-S2", "--size2", type=str, nargs=2, default=None,
                     help=_help["S2"], metavar=("MAJ2", "MIN2"))
    ps2.add_argument("-f1", "--flux_keys1", type=str, nargs=2, default=["int_flux", "peak_flux"],
                     help=_help["f1"], metavar=("S_INT_KEY1", "S_PEAK_KEY1"))
    ps2.add_argument("-f2", "--flux_keys2", type=str, nargs=2, default=["int_flux", "peak_flux"],
                     help=_help["f2"], metavar=("S_INT_KEY2", "S_PEAK_KEY2"))
    ps2.add_argument("--flux-ratio", "--flux_ratio", "--ratio", dest="ratio_flux",
                     type=float, default=1.2, help=_help["ratio_flux"])
    ps2.add_argument("--beam-ratio", "--beam_ratio", dest="ratio_beam",
                     type=float, default=-1., help=_help["ratio_beam"])


    args = ps.parse_args()

    if args.cite:
        print("Please cite 'Duchesne et al. in prep.' if this software is used "
              "for research work. Adding a link to the GitLab repository would "
              "also be appreciated: https://gitlab.com/Sunmish/flux_warp")
        sys.exit(0)

    logger.debug("opening cat1 {}".format(args.catalogue1))
    cat1 = Catalogue(args.catalogue1, 
                     ra_key=args.ra1, 
                     dec_key=args.dec1, 
                     flux_key=args.flux_key, 
                     eflux_key=args.eflux_key,
                     localrms_key=args.localrms_key,
                     a_key=args.size1[0],
                     b_key=args.size1[1],
                     psf_a_key=args.beam_key1[0],
                     psf_b_key=args.beam_key1[1],
                     pa_key=args.pa1)
    
    logger.debug("opening cat2 {}".format(args.catalogue2))
    if args.catalogue2 == _DEFAULT_MODEL_CATALOGUE:
        if not os.path.exists(_DEFAULT_MODEL_CATALOGUE):
            args.catalogue2 = get_data(_DEFAULT_MODEL_CATALOGUE)
            logger.debug("using {}".format(args.catalogue2))
    cat2 = Catalogue(args.catalogue2, ra_key=args.ra2, dec_key=args.dec2)


    # Use integrated/peak measurements for determining compact objects:
    if np.asarray([key in cat1.names for key in args.flux_keys1]).all() and \
        args.ratio_flux > 0.:
        logger.debug("excluding sources with S_int/S_peak > {} for {}".format(
            args.ratio_flux, cat1.name))
        logger.debug("len(cat1) with all sources {}".format(len(cat1.table)))
        cat1.compact_by_flux(int_flux=args.flux_keys1[0],
                             peak_flux=args.flux_keys1[1],
                             ratio=args.ratio_flux)
        logger.debug("len(cat1) with compact sources {}".format(len(cat1.table)))
    if np.asarray([key in cat2.names for key in args.flux_keys2]).all() and \
        args.ratio_flux > 0.:
        logger.debug("excluding sources with S_int/S_peak > {} for {}".format(
            args.ratio_flux, cat2.name))
        cat2.compact_by_flux(int_flux=args.flux_keys2[0],
                             peak_flux=args.flux_keys2[1],
                             ratio=args.ratio_flux)


    # Use source sizes to determine compact nature:
    if (args.beam1 is not None or None not in args.beam_key1) \
        and (None not in args.size1) and (args.ratio_beam > 0.):

        logger.debug("excluding sources with a*b/psfa*psfb > {} for {}".format(args.ratio_beam, cat1.name))
        logger.debug("len(cat1) with all sources {}".format(len(cat1.table)))

        if None not in args.beam_key1:
            beam1 = args.beam_key1
        else:
            beam1 = args.beam1

        cat1.compact_by_resolution(a=args.size1[0], b=args.size1[1], 
                                   bmaj=beam1[0], bmin=beam1[1], 
                                   ratio=args.ratio_beam)
        logger.debug("len(cat1) with compact sources {}".format(len(cat1.table)))

    if (args.beam2 is not None or args.beam_key2 is not None) \
        and (args.size2 is not None) and (args.ratio_beam > 0.):

        logger.debug("excluding sources with a*b/psfa*psfb > {} for {}".format(args.ratio_beam, cat2.name))
        logger.debug("len(cat2) with all sources {}".format(len(cat2.table)))
        
        if args.beam_key2 is not None:
            beam2 = args.beam_key2
        else:
            beam2 = args.beam2

        cat2.compact_by_resolution(a=args.size2[0], b=args.size2[1], 
                          bmaj=beam2[0], bmin=beam2[1], 
                          ratio=args.ratio_beam)
        logger.debug("len(cat2) with compact sources {}".format(len(cat2.table)))

    if args.coords is not None:

        # Only consider sources within some radius around the specified coords:
        logger.debug("excluding sources outside of {} degrees around {}".format(args.radius, args.coords))
        coords = SkyCoord(ra=args.coords[0], 
                          dec=args.coords[1],
                          unit=(u.deg, u.deg))
        cat1.only_within(coords, args.radius)
        cat2.only_within(coords, args.radius)

    logger.debug("clipping cat1...")
    cat1.clip(args.threshold)

    logger.info("sources in first catalogue: {}".format(len(cat1.table)))

    if args.separation is None:
        args.separation = 1.e30

    if args.zone > 0.:
        cat1.exclude_self(args.zone)

    logger.debug("matching catalogues...")
    indices = match(cat1, cat2, args.separation, args.zone)

    logger.info("total {} matches between {} and {}".format(len(indices), 
                                                            cat1.name, 
                                                            cat2.name))

    if args.outname is None:

        args.outname = os.path.basename(args.catalogue1).split(".")[0] + "_" + \
                       os.path.basename(args.catalogue2).split(".")[0] + ".fits"
        logger.info("setting output filename to {}".format(args.outname))

    write_out(cat1, cat2, indices, args.outname, nmax=args.nmax)


if __name__ == "__main__":
    main()



