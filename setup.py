#! /usr/bin/env python

import setuptools
import sys

with open("README.md", "r") as f:
    long_description = f.read()

if sys.version_info[0] == 3:
    _version = ">=3.0.0"
elif sys.version_info[0] == 2:
    _version = "<3.0.0"
else:
    _version = ""

reqs = [
    "astropy"+_version,
    "numpy",
    "scipy",
    "matplotlib"+_version,
    "psutil"
]

scripts = [
    "scripts/flux_warp",
    "scripts/match_catalogues",
    "scripts/snr_offset.py"
]

data = {
    "flux_warp":["data/GLEAM_EGC_params.fits"]
}

setuptools.setup(
    name="flux_warp",
    version="1.15.0",
    author="Stefan W Duchesne",
    author_email="stefanduchesne@gmail.com",
    description="A small package to warp the flux of a radio astronomy image.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/Sunmish/flux_warp",
    install_requires=reqs,
    packages=["flux_warp"],
    package_data=data,
    scripts=scripts,
)
