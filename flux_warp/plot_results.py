from __future__ import print_function, division

from distutils.spawn import find_executable

import numpy as np
from astropy.io import fits
from astropy.coordinates import SkyCoord, FK5, AltAz
from astropy import units as u
from astropy.wcs import WCS
from astropy.visualization import AsymmetricPercentileInterval, simple_norm

from flux_warp.models import TELESCOPES, Poly1D, LogX

# from flux_warp.flux_match import weighted_mean
# from scipy.optimize import curve_fit
from scipy.stats import norm as normal

import matplotlib as mpl

mpl.use("Agg")

import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec, SubplotSpec
from matplotlib.ticker import StrMethodFormatter
from matplotlib import cm
from matplotlib.colors import ListedColormap
from matplotlib.ticker import MaxNLocator

from matplotlib import rc

mpl.rcParams["xtick.direction"] = "in"
mpl.rcParams["ytick.direction"] = "in"

import logging

logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


COLORS = ["crimson", "dodgerblue", "black", "grey"]


def set_font(offset, uselatex=True):
    """Set some font properties."""

    if find_executable("latex") and uselatex:
        rc("font", **{"family": "serif", "serif": ["Times"], "weight": "medium"})
        rc("text", usetex=True)
        USE_LATEX = True  # define as global?
        ORDER = {
            0: r"$0^\mathrm{th}$",
            1: r"$1^\mathrm{st}$",
            2: r"$2^\mathrm{nd}$",
            3: r"$3^\mathrm{rd}$",
            4: r"$4^\mathrm{th}$",
            5: r"$5^\mathrm{th}$",
        }

        if offset:
            labelx1 = r"$S_\mathrm{image} - S_\mathrm{model}$"
            labelx2 = r"$\left(S_\mathrm{image} - S_\mathrm{model}\right) - \mathrm{M}$"
            labelx3 = labelx1
        else:
            labelx1 = r"$S_\mathrm{image} / S_\mathrm{model}$"
            labelx2 = r"$\log\left(S_\mathrm{image} / S_\mathrm{model}\right) - \log\left(\mathrm{M}\right)$"
            labelx3 = r"$\log\left(S_\mathrm{image} / S_\mathrm{model}\right)$"

    else:
        USE_LATEX = False
        ORDER = {0: "0th", 1: "1st", 2: "2nd", 3: "3rd", 4: "4th", 5: "5th"}

        if offset:
            labelx1 = "S(image) - S(model)"
            labelx2 = "[S(image) - S(model)] - model"
            labelx3 = labelx1
        else:
            labelx1 = "S(image) / S(model)"
            labelx2 = "log[S(image) / S(model)] - log(model)"
            labelx3 = "log[S(image) / S(model)]"

    return USE_LATEX, ORDER, labelx1, labelx2, labelx3


def make_divergent_cmap(cmap1, cmap2):
    """Get colormap that diverges based on two different colormaps."""
    top = cm.get_cmap(cmap1, 128)
    bottom = cm.get_cmap(cmap2, 128)
    newcolors = np.vstack((top(np.linspace(0, 1, 128)), bottom(np.linspace(0, 1, 128))))
    new_cmap = ListedColormap(newcolors, name="divmap")
    return new_cmap


def auto_v(pmin, pmax, data, stretch="linear"):
    """Determine vmin and vmax from AsymmetricPercentileInterval.

    A mirror of aplpy's old auto_v function.
    """

    interval = AsymmetricPercentileInterval(pmin, pmax)
    try:
        vmin, vmax = interval.get_limits(data)
    except IndexError:
        vmin = vmin = 0  # problem?

    normalizer = simple_norm(data, stretch=stretch, min_cut=vmin, max_cut=vmax)

    vmin = -0.1 * (vmax - vmin) + vmin
    logging.debug("New vmin: {:10.4e}".format(vmin))

    vmax = 0.1 * (vmax - vmin) + vmax
    logging.debug("New vmax: {:10.4e}".format(vmax))

    normalizer.vmin = vmin
    normalizer.vmax = vmax

    return vmin, vmax


# def determine_fluxscale_err(ratios, bins=40, weights=None):
#     """
#     """

#     hist, edges = np.histogram(ratios, bins=bins, weights=weights)
#     centres = [np.mean([edges[i], edges[i+1]]) for i in range(len(edges)-1)]

#     mean = np.nansum(hist*centres) / np.nansum(centres)
#     sigma = np.sqrt(np.nansum(hist * (centres - mean)**2) / np.nansum(hist))

#     try:
#         popt, pcov = curve_fit(gaussian, centres, hist,
#                                p0=[np.nanmax(hist), mean, sigma],
#                                maxfev=10000)
#         perr = np.sqrt(np.diag(pcov))
#     except RuntimeError:
#         logger.warning("No Gaussian function could be fit!")
#         popt = perr = np.full((3,), np.nan)

#     return popt, perr


def complement(rgb):
    """Get complementary color from rgb tuple.

    from https://stackoverflow.com/a/40234924/6058788
    """
    k = sum([min(rgb), max(rgb)])
    return tuple(k - c for c in rgb)


class MidpointNormalize(mpl.colors.Normalize):
    """Get linear normalization with a midpoint.
    https://stackoverflow.com/questions/20144529/shifted-colorbar-matplotlib/20146989#20146989
    """

    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        mpl.colors.Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        # I'm ignoring masked values and all kinds of edge cases to make a
        # simple example...
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y))


# def get_axes(n, gs, fig):
#     """
#     """

#     axes = []
#     for i in range(n):
#         sb = SubplotSpec(gs, i)
#         try:
#             sp = sb.get_position(figure=fig).get_points().flatten()
#         except KeyError:
#             sp = sb.get_position(fig=fig).get_points().flatten()
#         x = sp[0]
#         y = sp[1]
#         dx = sp[2]-x
#         dy = sp[3]-y
#         axes.append([x, y, dx, dy])

#     return axes


def plot_map(
    fluxmatch,
    header,
    outname,
    cmap="gnuplot2",
    data="ratio",
    image=None,
    uselatex=True,
    use_midpoint_norm=False,
):
    """Plot correction factor map.

    Parameters
    ----------
    fluxmatch : flux_warp.flux_match.FluxMatch 
        FluxMatch object after running all necessary methods.
    header : astropy.io.fits.Header
        Header requried for WCS information. 
    outname : str
        Output filename.
    cmap : str, optional
        Name of colormap to use for ratios/offsets/residuals. [Default 'gnuplot2']
    data : str, optional
        Name of field in FluxMatch.mdata for plotting. One of {'ratio', 
        'residual'}. [Default 'ratio']
    image : str, optional
        Name of FITS image to plot as background map. Factor map from FluxMatch
        is not plotted if this is specified. [Default None]
    uselatex : bool, optional
        Select True if wanting to use LaTeX for fonts. [Default True]
    use_midpoint_norm : bool, optional
        Use a normalisation that has a midpoint defined by the mean depending
        on data. Useful for diverging colormaps. [Default False]

    """

    USE_LATEX, _, _labelx1, _labelx2, labelx3 = set_font(fluxmatch.offset, uselatex)

    if data == "ratio":
        labelx = _labelx1
    else:
        labelx = _labelx2

    cmap = plt.get_cmap(cmap)
    COLORS = ["black", "black"]

    plt.close("all")
    font_labels = 24
    font_ticks = 22
    markersize = 150

    figsize = (9, 10)
    R = figsize[0] / figsize[1]
    axes = [0.02, 0.02 * R, 0.96, 0.88 - 0.02 * R]
    r = 8.0 / 10.0
    cbax = [axes[0], axes[1] + axes[3] + 0.01, axes[2], 0.03 * 0.8 * r]

    wcs = WCS(header).celestial
    vmin, vmax = fluxmatch.get_minmax(data)
    if use_midpoint_norm:
        norm = MidpointNormalize(
            vmin=vmin, vmax=vmax, midpoint=np.mean(fluxmatch.mdata[data])
        )
    else:
        norm = mpl.colors.Normalize(vmin=vmin, vmax=vmax)

    fig = plt.figure(figsize=figsize)
    ax1 = plt.axes(axes, projection=wcs)

    if image is None:
        image_data = fluxmatch.factors
        image_vset = (vmin, vmax)
        image_norm = norm
        image_cmap = cmap
    else:
        image_data = np.squeeze(image)
        image_vset = auto_v(0.25, 99.5, np.squeeze(image_data))
        image_norm = mpl.colors.Normalize(vmin=image_vset[0], vmax=image_vset[1])
        image_cmap = "gray"

    ax1.imshow(
        image_data, cmap=image_cmap, norm=image_norm, origin="lower", aspect="auto"
    )
    ax1.scatter(
        fluxmatch.mdata.ra,
        fluxmatch.mdata.dec,
        c=fluxmatch.mdata[data],
        edgecolors=COLORS[0],
        s=markersize,
        marker="o",
        cmap=cmap,
        transform=ax1.get_transform("world"),
        label="Calibrators",
    )
    labels = [
        mpl.lines.Line2D(
            [],
            [],
            color=COLORS[0],
            marker="o",
            linestyle="",
            markersize=np.sqrt(markersize),
            fillstyle="none",
            label="Calibrators",
        )
    ]
    if fluxmatch.tdata is not None:
        ax1.scatter(
            fluxmatch.tdata.ra,
            fluxmatch.tdata.dec,
            c=fluxmatch.tdata[data],
            edgecolors=COLORS[1],
            s=markersize,
            marker="s",
            cmap=cmap,
            transform=ax1.get_transform("world"),
            label="Test set",
        )
        labels.append(
            mpl.lines.Line2D(
                [],
                [],
                color=COLORS[1],
                marker="s",
                linestyle="",
                markersize=np.sqrt(markersize),
                fillstyle="none",
                label="Test set",
            )
        )

    legend = ax1.legend(
        handles=labels,
        loc="best",
        shadow=False,
        fancybox=False,
        frameon=True,
        fontsize=font_labels - 2,
        numpoints=1,
        framealpha=1.0,
        facecolor="white",
    )
    legend.get_frame().set_edgecolor("black")

    ax1.set_xlim([0, header["NAXIS1"]])
    ax1.set_ylim([0, header["NAXIS2"]])

    for i in range(2):
        ax1.coords[i].set_ticklabel_visible(False)
        ax1.coords[i].display_minor_ticks(True)
        ax1.coords[i].set_minor_frequency(5)
        ax1.coords[i].grid(color="black", linestyle="--")

    cax = fig.add_axes(cbax)
    cb1 = mpl.colorbar.ColorbarBase(cax, cmap=cmap, norm=norm, orientation="horizontal")
    cb1.set_label(
        labelx,
        fontsize=font_labels - 2,
        labelpad=10.0,
        verticalalignment="top",
        horizontalalignment="center",
    )
    cb1.ax.tick_params(
        which="major",
        labelsize=font_ticks - 2,
        length=5.0,
        color="black",
        labelcolor="black",
        width=1.0,
    )
    cb1.ax.xaxis.set_ticks_position("top")
    cb1.ax.xaxis.set_label_position("top")

    # too many ticks increases chance of ticks bleeding off axis edge
    cb1.locator = MaxNLocator(4)
    cb1.update_ticks()

    fig.savefig(outname, dpi=72, bbox_inches="tight")


def plot_hist(fluxmatch, outname, gaussians=True, uselatex=True):
    """Plot histogram of calibrator ratios/offsets.

    Parameters
    ----------
    fluxmatch : flux_warp.flux_match.FluxMatch 
        FluxMatch object after running all necessary methods.
    outname : str
        Output filename.
    gaussians : bool, optional
        Select True if wanting to look at the fitted probability distribution
        functions. [Default True]
    uselatex : bool, optional
        Select True if wanting to use LaTeX for fonts. [Default True]

    """

    USE_LATEX, _, _, _, labelx3 = set_font(fluxmatch.offset, uselatex)

    colors = ["crimson", "dodgerblue", "black", "grey"]

    plt.close("all")
    font_labels = 24
    font_ticks = 22

    figsize = (9, 7)
    R = figsize[0] / figsize[1]
    axes = [0.15 / R, 0.12, 1.0 - 0.15 / R - 0.02, 1 - 0.12 - 0.02 * R]
    fig = plt.figure(figsize=figsize)
    ax1 = plt.axes(axes)

    # if USE_LATEX:
    #     if fluxmatch.offset:
    #         labelx = r"$S_\mathrm{measured} - S_\mathrm{predicted}$"
    #     else:
    #         labelx = r"$S_\mathrm{measured} / S_\mathrm{predicted}$"
    # else:
    #     if fluxmatch.offset:
    #         labelx = "Sm - Sp"
    #     else:
    #         labelx = "Sm/Sp"

    vmin1, vmax1 = fluxmatch.get_minmax("logratio")
    vmin2, vmax2 = fluxmatch.get_minmax("residual")
    vmin = min([vmin1, vmin2])
    vmax = max([vmax1, vmax2])

    ax1.hist(
        fluxmatch.mdata.logratio,
        bins=40,
        histtype="step",
        color=COLORS[0],
        fill=False,
        density=True,
        lw=2.0,
    )
    ax1.hist(
        fluxmatch.mdata.residual,
        bins=40,
        histtype="step",
        color=COLORS[1],
        fill=False,
        density=True,
        lw=2.0,
    )

    labels = ["Calibrators", "Cal. residuals"]

    if fluxmatch.tdata is not None:
        ax1.hist(
            fluxmatch.tdata.logratio,
            bins=40,
            histtype="step",
            color=COLORS[2],
            fill=False,
            density=True,
            lw=2.0,
            ls="--",
        )
        ax1.hist(
            fluxmatch.tdata.residual,
            bins=40,
            histtype="step",
            color=COLORS[3],
            fill=False,
            density=True,
            lw=2.0,
            ls="--",
        )

        labels.append("Test subset")
        labels.append("Test residuals")

    if gaussians:
        mu1, std1 = normal.fit(fluxmatch.mdata.logratio)
        mu2, std2 = normal.fit(fluxmatch.mdata.residual)

        if USE_LATEX:
            labels[0] += ": $\\sigma = {:.3f}$, $\\mu = {:.3f}$".format(std1, mu1)
            labels[1] += ": $\\sigma = {:.3f}$, $\\mu = {:.3f}$".format(std2, mu2)
        else:
            labels[0] += ": std = {:.3f}, mu = {:.3f}".format(std1, mu1)
            labels[1] += ": std = {:.3f}, mu = {:.3f}".format(std2, mu2)

        x = np.linspace(vmin, vmax, 1000)

        ax1.plot(x, normal.pdf(x, mu1, std1), color=COLORS[0], ls="-", lw=2.0)
        ax1.plot(x, normal.pdf(x, mu2, std2), color=COLORS[1], ls="-", lw=2.0)

        if fluxmatch.tdata is not None:
            mu3, std3 = normal.fit(fluxmatch.tdata.logratio)
            mu4, std4 = normal.fit(fluxmatch.tdata.residual)
            ax1.plot(x, normal.pdf(x, mu3, std3), color=COLORS[2], ls="--", lw=2.0)
            ax1.plot(x, normal.pdf(x, mu4, std4), color=COLORS[3], ls="--", lw=2.0)

            if USE_LATEX:
                labels[2] += ": $\\sigma = {:.3f}$, $\\mu = {:.3f}$".format(std3, mu3)
                labels[3] += ": $\\sigma = {:.3f}$, $\\mu = {:.3f}$".format(std4, mu4)
            else:
                labels[2] += ": std = {:.3f}, mu = {:.3f}".format(std3, mu3)
                labels[3] += ": std = {:.3f}, mu = {:.3f}".format(std4, mu4)

    ax1.set_xlim([vmin, vmax])
    ax1.set_xlabel(labelx3, fontsize=font_labels)
    ax1.set_ylabel("Probability density", fontsize=font_labels)
    ax1.tick_params(axis="both", length=7.0, labelsize=font_ticks)

    ax1.yaxis.set_major_locator(MaxNLocator(integer=True))
    ax1.xaxis.set_major_locator(MaxNLocator(4))

    handles = [
        mpl.lines.Line2D(
            [], [], color=COLORS[0], linestyle="-", lw=2.0, label=labels[0]
        ),
        mpl.lines.Line2D(
            [], [], color=COLORS[1], linestyle="-", lw=2.0, label=labels[1]
        ),
    ]
    if not fluxmatch.tdata is None:
        handles.append(
            mpl.lines.Line2D(
                [], [], color=COLORS[2], linestyle="--", lw=2.0, label=labels[2]
            )
        )
        handles.append(
            mpl.lines.Line2D(
                [], [], color=COLORS[3], linestyle="--", lw=2.0, label=labels[3]
            )
        )

    legend1 = ax1.legend(
        handles=handles,
        loc="best",
        shadow=False,
        fancybox=False,
        frameon=True,
        fontsize=font_ticks - 2,
        numpoints=1,
    )
    legend1.get_frame().set_edgecolor("black")

    fig.savefig(outname, bbox_inches="tight")
    plt.close("all")


def plot_snr_residuals(fluxmatch, outname, uselatex=True):
    """Plot residuals from model/calibrators.

    Parameters
    ----------
    fluxmatch : flux_warp.flux_match.FluxMatch 
        FluxMatch object after running all necessary methods.
    outname : str
        Output filename.
    uselatex : bool, optional
        Select True if wanting to use LaTeX for fonts.

    """

    USE_LATEX, _, labelx1, labelx2, _ = set_font(fluxmatch.offset, uselatex)

    plt.close("all")
    font_labels = 24.0
    font_ticks = 22.0

    figsize = (9, 8)
    R = figsize[0] / figsize[1]
    ypos = 0.12 * (7.0 / 8.0) + (2 * 1.0 / 8.0) + 0.01  # this is a bit mad
    axes = [0.15 / R, ypos, 1.0 - 0.15 / R - 0.02, 1.0 - 0.02 * R - ypos]
    axer = [0.15 / R, 0.12 * (7.0 / 8.0), 1.0 - 0.15 / R - 0.02, 2 * 1.0 / 8.0 - 0.01]

    print(axes)
    print(axer)

    fig = plt.figure(figsize=figsize)
    ax1 = plt.axes(axes)
    axr = plt.axes(axer)

    ax1.plot(
        fluxmatch.mdata.snr,
        fluxmatch.mdata.ratio,
        ls="",
        marker="o",
        c=COLORS[0],
        zorder=2,
        label="Calibrators",
    )
    ax1.plot(
        fluxmatch.mdata.snr,
        fluxmatch.mdata.model,
        ls="",
        marker="o",
        c=COLORS[1],
        zorder=2,
        label="Cal. model",
    )
    axr.plot(
        fluxmatch.mdata.snr,
        fluxmatch.mdata.residual,
        ls="",
        marker="o",
        c=COLORS[0],
        zorder=2,
    )
    ax1.axhline(fluxmatch.weighted_mean, ls="-", c="black")
    ax1.axhline(fluxmatch.median, ls="--", c="black")
    axr.axhline(0.0, ls="-", c="black")

    if fluxmatch.tdata is not None:
        ax1.plot(
            fluxmatch.tdata.snr,
            fluxmatch.tdata.ratio,
            ls="",
            marker="s",
            c=COLORS[2],
            zorder=2,
            label="Test subset",
        )
        ax1.plot(
            fluxmatch.tdata.snr,
            fluxmatch.tdata.model,
            ls="",
            marker="s",
            c=COLORS[3],
            zorder=2,
            label="Test model",
        )
        axr.plot(
            fluxmatch.tdata.snr,
            fluxmatch.tdata.residual,
            ls="",
            marker="s",
            c=COLORS[2],
            zorder=2,
        )

    # if fluxmatch.snr_popt is not None:
    #     vmin, vmax = fluxmatch.get_minmax("snr")
    #     _x = np.linspace(vmin, vmax, 1000)
    #     _y = LogX.evaluate(_x, *fluxmatch.snr_popt)
    #     ax1.plot(_x, _y, ls="-", c=COLORS[-1], zorder=10, )

    ax1.set_xscale("log")
    axr.set_xscale("log")

    legend1 = ax1.legend(
        loc="best",
        shadow=False,
        fancybox=False,
        frameon=True,
        fontsize=font_ticks - 4,
        numpoints=1,
    )
    legend1.get_frame().set_edgecolor("black")

    ax1.set_ylabel(labelx1, fontsize=font_labels)
    ax1.set_xlabel("")
    axr.set_ylabel(labelx2, fontsize=font_ticks - 2)
    axr.set_xlabel("SNR", fontsize=font_labels)

    ax1.tick_params(labelsize=font_ticks, length=7.0)
    ax1.tick_params(which="minor", length=5.0)
    ax1.xaxis.set_ticklabels([])
    axr.tick_params(labelsize=font_ticks, length=7.0, axis="x", pad=7)
    axr.tick_params(labelsize=font_ticks - 4, length=7.0, axis="y")
    axr.tick_params(which="minor", length=5.0)

    fig.savefig(outname, bbox_inches="tight")
    plt.close("all")


def plot_sigma_clip(fluxmatch, outname, uselatex=True):
    """Plot the sigma/and source-clipping process.

    Parameters
    ----------
    fluxmatch : flux_warp.flux_match.FluxMatch 
        FluxMatch object after running all necessary methods.
    outname : str
        Output filename.
    uselatex : bool, optional
        Select True if wanting to use LaTeX for fonts.

    """

    USE_LATEX, _, labelx1, labelx2, _ = set_font(fluxmatch.offset, uselatex)

    plt.close("all")
    font_labels = 24
    font_ticks = 22

    figsize = (9, 7)
    R = figsize[0] / figsize[1]
    axes = [0.15 / R, 0.12, 1.0 - 0.15 / R - 0.02, 1 - 0.12 - 0.02 * R]
    fig = plt.figure(figsize=figsize)
    ax1 = plt.axes(axes)

    data = fluxmatch.all_ratios
    labels = ["Original"]
    if USE_LATEX:
        labels.append(r"$\sigma$-clipped")
        labels.append(r"$N_\mathrm{src}$ cut/final")
    else:
        labels.append("sigma-clipped")
        labels.append("N. cut/final")

    if fluxmatch.tdata is not None:
        data.append(fluxmatch.tdata.ratio)
        labels.append("Test sources")

    ax1.boxplot(data, medianprops={"color": COLORS[0]}, vert=True)

    ax1.set_xticklabels(labels)
    ax1.set_xlabel("Samples", fontsize=font_labels)
    ax1.set_ylabel(labelx1, fontsize=font_labels)

    ax1.tick_params(axis="both", which="both", labelsize=font_ticks)
    ax1.tick_params(axis="x", which="major", pad=7.0)

    fig.savefig(outname, bbox_inches="tight")
    plt.close("all")


def plot_poly1d(fluxmatch, outname, uselatex=True, time=None, telescope="MWA"):
    """Plot Poly1D model if it has been fit.
    
    Parameters
    ----------
    fluxmatch : flux_warp.flux_match.FluxMatch 
        FluxMatch object after running all necessary methods.
    outname : str
        Output filename.
    uselatex : bool, optional
        Select True if wanting to use LaTeX for fonts.

    """

    USE_LATEX, ORDER, labelx1, labelx2, _ = set_font(fluxmatch.offset, uselatex)

    plt.close("all")
    font_labels = 24
    font_ticks = 22

    figsize = (9, 7)
    R = figsize[0] / figsize[1]
    axes = [0.15 / R, 0.12, 1.0 - 0.15 / R - 0.02, 1 - 0.12 - 0.02 * R]
    fig = plt.figure(figsize=figsize)
    ax1 = plt.axes(axes)

    if fluxmatch.model.mtype == "declination":
        coordinate = fluxmatch.mdata.dec
        if fluxmatch.tdata is not None:
            tcoordinate = fluxmatch.tdata.dec
        labelx = "Declination (J2000)"
    elif telescope is not None and time is not None:
        radec = FK5(ra=fluxmatch.mdata.ra * u.deg, dec=fluxmatch.mdata.dec * u.deg)
        altaz = radec.transform_to(AltAz(obstime=time, location=TELESCOPES[telescope]))
        coordinate = altaz.alt.value
        if fluxmatch.tdata is not None:
            tradec = FK5(ra=fluxmatch.tdata.ra * u.deg, dec=fluxmatch.tdata.dec * u.deg)
            taltaz = tradec.transform_to(
                AltAz(obstime=time, location=TELESCOPES[telescope])
            )
            tcoordinate = taltaz.alt.value
        labelx = "Elevation"
    else:
        raise RuntimeError("no telescope and time information provided!")

    if fluxmatch.tdata is not None:
        vmin = min([min(tcoordinate), min(coordinate)])
        vmax = max([max(tcoordinate), max(coordinate)])
    else:
        vmin = min(coordinate)
        vmax = max(coordinate)

    x = np.linspace(vmin, vmax, 1000)
    y = 10 ** fluxmatch.model.evaluate(x, *fluxmatch.model.params)

    ax1.plot(
        coordinate,
        fluxmatch.mdata.ratio,
        marker="o",
        ms=7.0,
        c=COLORS[0],
        ls="",
        label="Calibrators",
    )
    if fluxmatch.tdata is not None:
        ax1.plot(
            tcoordinate,
            fluxmatch.tdata.ratio,
            marker="s",
            ms=7.0,
            c=COLORS[2],
            ls="",
            label="Test subset",
        )
    ax1.plot(
        x,
        y,
        ls="-",
        lw=2.0,
        c=COLORS[1],
        zorder=10,
        label="{} order polynomial".format(ORDER[len(fluxmatch.model.params) - 1]),
    )

    legend1 = ax1.legend(
        loc="best",
        shadow=False,
        fancybox=False,
        frameon=True,
        fontsize=font_ticks - 4,
        numpoints=1,
        facecolor="white",
        framealpha=1.0,
    )
    legend1.get_frame().set_edgecolor("black")

    ax1.set_ylabel(labelx1, fontsize=font_labels)
    ax1.set_xlabel(labelx, fontsize=font_labels)
    ax1.tick_params(labelsize=font_ticks, length=7.0)
    ax1.xaxis.set_major_formatter(StrMethodFormatter("{x:+.0f}"))

    fig.savefig(outname, bbox_inches="tight")
    plt.close("all")


def plot_snr_logx(snr, offsets, params, outname, uselatex=True):
    """
    """

    USE_LATEX, _, labely, _, _ = set_font(True, uselatex)

    plt.close("all")

    plt.close("all")
    font_labels = 24
    font_ticks = 22

    figsize = (9, 7)
    R = figsize[0] / figsize[1]
    axes = [0.2 / R, 0.12, 1.0 - 0.2 / R - 0.02, 1 - 0.12 - 0.02 * R]
    fig = plt.figure(figsize=figsize)
    ax1 = plt.axes(axes)

    labelx = "SNR"
    x = np.linspace(min(snr), max(snr), 1000)
    y = LogX().evaluate(x, *params)

    if USE_LATEX:
        fit_label = "$\\Delta = {:.4f}\\log(\\mathrm{}) {:+.4f}$".format(
            params[0], r"{SNR}", params[1]
        )
    else:
        fit_label = "d = {:.4f}log(SNR){:+.4f}".format(params[0], params[1])

    ax1.plot(snr, offsets, marker="o", ms=7.0, c=COLORS[0], ls="", label="Sources")
    ax1.plot(x, y, ls="-", lw=2.0, c=COLORS[1], zorder=10, label=fit_label)

    ax1.set_xscale("log")

    legend1 = ax1.legend(
        loc="best",
        shadow=False,
        fancybox=False,
        frameon=True,
        fontsize=font_ticks - 4,
        numpoints=1,
        facecolor="white",
        framealpha=1.0,
    )
    legend1.get_frame().set_edgecolor("black")

    ax1.set_ylabel(labely, fontsize=font_labels)
    ax1.set_xlabel(labelx, fontsize=font_labels)
    ax1.tick_params(labelsize=font_ticks, length=7.0)

    fig.savefig(outname, bbox_inches="tight")
    plt.close("all")


# def plot(factors, header, pra, pdec, ratios, cmap="cubehelix",
#          outname=None, beam_image=None, centre=None,
#          radius=None, model=None, time=None, telescope=None,
#          use_midpoint_norm=False, snr=None,
#          offset=False, uselatex=True):
#     """Plot used for QA of fluxscale corrections.

#     Parameters
#     ----------
#     correction_image : str
#         Filename of a FITS image that has correction factors, output from
#         `~flux_warp.fluxmatch.apply_corrections`.
#     pra : array-like
#         1-D array of RAs for calibrator sources.
#     pdec : array-like
#         1-D array of DECs corresponding to ``pra``.
#     ratios : tuple of arrays
#         3 1-D array of ratios corresponding to ``pra`` for original, clipped,
#         and post-clipped ratios for box and whisker plot.
#     cmap : str, optional
#         Colormap to use. [Default 'cubehelix']
#     outname : str, optional
#         Output filename for the plot. If not supplied, the image name will be
#         ``correction_image`` without the .fits extension in .eps format.
#     plot_beam : bool, optional
#         Select ``True`` if wanting to plot the beam image in place of the
#         histogram image. [Default False]
#     beam_image : str, optional
#         If ``plot_beam``, this specifies the beam image to use. [Default None]
#     centre : tuple, float, optional
#         Tuple of RA,DEC for plotting a circle when used with ``radius``.
#         [Default None]
#     radius : float, optional
#         If specified with ``centre``, a circle will be drawn of this size
#         on the sky plots. [Default None]
#     dec_model : `~flux_warp.models.Poly1D object, optional
#         A `~flux_warp.models.Poly1D object with filled-in parameters.
#         If supplied, this will be plotted in the lower right corner.
#         [Default None]


#     """


#     USE_LATEX, ORDER = set_font(uselatex)

#     # correction_map = np.squeeze(fits.getdata(correction_image))
#     correction_map = factors
#     correction_factor_mean = np.nanmean(correction_map)

#     # Use discrete colormaps to highlight changes better:
#     nan_color = "black"
#     beam_cmap = plt.get_cmap("gray_r", 21)
#     cmap = plt.get_cmap(cmap, 21)
#     cmap.set_bad(color=nan_color)

#     # Choose appropriate colors for points/lines/etc:
#     colors = [cmap(4), cmap(14)]
#     if cmap.name in ["Spectral", "Spectral_r"]:
#         comp_color = "black"
#     else:
#         comp_color = complement(cmap(11))[:-1]
#     point_color = "black"
#     circle_color = comp_color

#     # Start setting up the plot:
#     plt.close("all")

#     font_labels = 18.
#     font_ticks = 16.
#     R = 1.

#     figsize = (12, 8)
#     fig = plt.figure(figsize=figsize)

#     gs = GridSpec(2, 3,
#                   wspace=0.08,
#                   hspace=0.22,
#                   left=0.02,
#                   right=1-0.07,
#                   top=1.-0.02,
#                   bottom=0.1)

#     ax1 = plt.subplot(gs[:, :-1])
#     ax2 = plt.subplot(gs[0, -1])
#     ax3 = plt.subplot(gs[1, -1])

#     # Certain labels will need to be changed if no latex is used:
#     if USE_LATEX:
#         _label1 = r"$\sigma$-clip"
#         _label2 = r"$N_{\mathrm{src}}$ cut"
#         if offset:
#             _labelx = r"$S_\mathrm{measured} - S_\mathrm{predicted}$"
#         else:
#             _labelx = r"$S_\mathrm{measured}/S_\mathrm{predicted}$"
#     else:
#         _label1 = "Clipped"
#         _label2 = "N cut"
#         if offset:
#             _labelx = "S(measured) - S(predicted)"
#         else:
#             _labelx = "S(measured) / S(predicted)"

#     ax2.boxplot(ratios, medianprops={"color": colors[0]}, vert=False)
#     ax2.yaxis.tick_right()
#     ax2.tick_params(axis="both", which="both", labelsize=font_ticks)
#     ax2.tick_params(axis="y", which="major", pad=7)

#     labels = [item.get_text() for item in ax2.get_yticklabels()]
#     labels[0] = "Original"
#     labels[1] = _label1
#     labels[2] = _label2
#     ax2.set_yticklabels(labels, rotation=90, va="center")
#     ax2.set_xlabel(_labelx, fontsize=font_labels)
#     ax2.set_ylabel(r"Samples", fontsize=font_labels)
#     ax2.yaxis.set_label_position("right")

#     # hdr = fits.getheader(correction_image)
#     hdr = header
#     wcs = WCS(hdr).celestial

#     if radius is not None and centre is not None:

#         cd1 = None
#         if "CD1_1" in hdr.keys():
#             cd1 = abs(hdr["CD1_1"])
#         elif "CDELT1" in hdr.keys():
#             cd1 = abs(hdr["CDELT1"])
#         if cd1 is not None:
#             ra, dec = centre
#             circle_size = int(radius/cd1)
#             ra_pix, dec_pix = wcs.all_world2pix(ra, dec, 0)
#             c1 = plt.Circle((ra_pix, dec_pix), circle_size, color=circle_color,
#                             fill=False, linewidth=2)
#             c3 = plt.Circle((ra_pix, dec_pix), circle_size, color=circle_color,
#                             fill=False, linewidth=2)
#         else:
#             c1 = c3 = None
#     else:
#         c1 = c3 = None


#     # map_ratios = fits.getdata(correction_image).flatten()
#     map_ratios = correction_map.flatten()
#     mean_ratio = np.nanmean(map_ratios)

#     params, err_params = determine_fluxscale_err(ratios[2], weights=snr)
#     if np.isnan(params[2]):
#         logger.warning("setting sigma to 0.")
#         sigma_ratio = 0.
#     else:
#         sigma_ratio = abs(params[2])
#     # err_sigma_ratio = err_params[2]
#     err_sigma_ratio = np.nanmean(ratios[2])/np.sqrt(len(ratios[2]))

#     if model is not None:

#         # Here we will plot the dec-dependent model in the bottom corner:

#         if model.mtype == "declination":

#             coordinate = np.asarray(pdec)
#             _labely = "Declination (J2000)"

#         elif model.mtype == "elevation":

#             if telescope is None or time is None:
#                 raise ValueError("A Time object and telescope name must "
#                                  "be supplied if supplying an elevation-"
#                                  "dependent model.")

#             radec = FK5(ra=np.asarray(pra)*u.deg, dec=np.asarray(pdec)*u.deg)
#             altaz = radec.transform_to(AltAz(obstime=time,
#                                              location=TELESCOPES[telescope]))

#             coordinate = altaz.alt.value
#             _labely = "Elevation"

#         arr = np.array([coordinate, ratios[2]]).T
#         arr = arr[arr[:, 0].argsort()]

#         ax3.plot(arr[:, 0], arr[:, 1], c=point_color, mec=point_color,
#                  marker="o", ms=3., ls="", mfc=point_color,
#                  label="Calibrator sources", alpha=1.)
#         r_model = model.evaluate(arr[:, 0], *model.params)
#         ax3.plot(arr[:, 0], r_model, c=colors[1], ls="-", marker="",
#                  label=r"{} order polynomial".format(ORDER[len(model.params)-1]))

#         ax3.set_xlabel(_labely, fontsize=font_labels)
#         ax3.set_ylabel(_labelx, fontsize=font_labels)
#         legend = ax3.legend(loc="upper right", shadow=False, fancybox=False,
#                                 frameon=True, fontsize=14, numpoints=1)
#         legend.get_frame().set_edgecolor("dimgrey")
#         ax3.yaxis.tick_right()
#         ax3.yaxis.set_label_position("right")
#         ax3.xaxis.set_major_formatter(StrMethodFormatter("{x:+.0f}"))
#         ax3.tick_params(axis="both", which="both", labelsize=16.)


#     elif beam_image is not None:


#         # Here we will use the supplied beam image and plot it in the corner.
#         # This can be useful as the flux density issues tend to follow the
#         # primary beam response pattern with the MWA.

#         beam_map = fits.getdata(beam_image)
#         sb1 = SubplotSpec(gs, 5)
#         sp1 = sb1.get_position(fig).get_points().flatten()
#         x = sp1[0]
#         dx = sp1[2] - x
#         sb2 = SubplotSpec(gs, 5)
#         sp2 = sb2.get_position(fig).get_points().flatten()
#         y = sp2[1]

#         cbax = [x, y-0.025, dx, 0.02]

#         norm_beam = mpl.colors.Normalize(vmin=np.nanmin(beam_map),
#                                          vmax=np.nanmax(beam_map))

#         ax3.imshow(np.squeeze(beam_map), cmap=beam_cmap, origin="lower",
#                    aspect="auto", norm=norm_beam)

#         ax3.axes.get_xaxis().set_ticks([])
#         ax3.axes.get_yaxis().set_ticks([])

#         colorbar_axis_beam = fig.add_axes(cbax)
#         colorbar_beam = mpl.colorbar.ColorbarBase(colorbar_axis_beam,
#                                                   cmap=beam_cmap,
#                                                   norm=norm_beam,
#                                                   orientation="horizontal")

#         beam_label = "Model Stokes I attenuation"
#         colorbar_beam.set_label(beam_label,
#                                 fontsize=font_labels,
#                                 labelpad=0.,
#                                 verticalalignment="top",
#                                 horizontalalignment="center")
#         colorbar_beam.ax.tick_params(which="major",
#                                      labelsize=font_ticks,
#                                      length=5.,
#                                      color="black",
#                                      labelcolor="black",
#                                      width=1.)

#         if c3 is not None:
#             ax3.add_artist(c3)

#     else:

#         all_ratios = ratios[2]
#         all_colors = colors[1]
#         all_labels = "Calibrators"

#         ax3.hist(all_ratios, bins=40, histtype="step", color=all_colors,
#                  fill=False, label=all_labels, density=False)
#         gauss_model = np.linspace(np.nanmin(ratios[2]), np.nanmax(ratios[2]), 1000)

#         if USE_LATEX:
#             gauss_label = "$\\sigma = {:.2f}$".format(sigma_ratio)
#         else:
#             gauss_label = "Unc. = {:.2f}".format(sigma_ratio)

#         if np.isfinite(params).all():
#             ax3.plot(gauss_model, gaussian(gauss_model, *params), ls="-",
#                      c=colors[1], label=gauss_label)

#         legend = ax3.legend(loc="best", shadow=False, fancybox=False,
#                             frameon=True, fontsize=16, numpoints=1)
#         legend.get_frame().set_edgecolor("dimgrey")

#         ax3.set_xlabel(_labelx, fontsize=font_labels)
#         ax3.tick_params(axis="both", which="both", labelsize=14.)
#         ax3.yaxis.tick_right()


#     if not use_midpoint_norm:
#         norm = mpl.colors.Normalize(vmin=min(ratios[2]), vmax=max(ratios[2]))
#     else:
#         norm = MidpointNormalize(vmin=min(ratios[2]), vmax=max(ratios[2]),
#                                  midpoint=np.nanmean(ratios[2]))

#     sb1 = SubplotSpec(gs, 0)
#     sp1 = sb1.get_position(fig).get_points().flatten()
#     x = sp1[0]

#     gp1 = ax1.get_position().get_points().flatten()
#     dx = gp1[2] - gp1[0]
#     sb2 = SubplotSpec(gs, 4)
#     sp2 = sb2.get_position(fig).get_points().flatten()
#     y = sp2[1]
#     cbax = [x, y-0.025, dx, 0.02]

#     ax1.imshow(correction_map, cmap=cmap, norm=norm,
#                origin="lower", aspect="auto")

#     x, y = wcs.all_world2pix(pra, pdec, 0)

#     ax1.scatter(x, y, c=ratios[2], edgecolors=circle_color, s=100, marker="o",
#                 norm=norm, cmap=cmap)

#     ax1.axes.get_xaxis().set_ticks([])
#     ax1.axes.get_yaxis().set_ticks([])

#     ax1.set_xlim([min(x), max(x)])
#     ax1.set_ylim([min(y), max(y)])

#     colorbar_axis = fig.add_axes(cbax)
#     colorbar = mpl.colorbar.ColorbarBase(colorbar_axis, cmap=cmap, norm=norm,
#                                          orientation="horizontal")
#     colorbar.set_label(_labelx,
#                        fontsize=font_labels,
#                        labelpad=0.,
#                        verticalalignment="top",
#                        horizontalalignment="center")
#     colorbar.ax.tick_params(which="major",
#                             labelsize=font_ticks,
#                             length=5.,
#                             color="black",
#                             labelcolor="black",
#                             width=1.)

#     if c1 is not None:
#         ax1.add_artist(c1)

#     if outname is None:
#         # default output format is pdf as it preserve transparency.
#         # png also does this, but pdf is a vector format.
#         outname = correction_image.replace(".fits", ".pdf")

#     fig.savefig(outname, dpi=300)


# # def plot_all(factors, header, ratios, pra, pdec, cmap1="cubehelix",
#              # cmap2="Spectral_r", tratios=None, tra=None, tdec=None,
#              # outname=None, model=None, offset=False, uselatex=True):
# def plot_all(fluxmatch, header, cmap1="PRGn", cmap2="Spectral_r",
#              outname=None, uselatex=True):
#     """
#     """

#     USE_LATEX, _ = set_font(uselatex)

#     if USE_LATEX:
#         if fluxmatch.offset:
#             _labelx1 = r"$S_\mathrm{measured} - S_\mathrm{predicted}$"
#             _labelx2 = r"$(S_\mathrm{measured} - S_\mathrm{predicted}) - \mathrm{CF}$"
#         else:
#             _labelx1 = r"$S_\mathrm{measured} / S_\mathrm{predicted}$"
#             _labelx2 = r"$S_\mathrm{measured} / S_\mathrm{predicted} - \mathrm{CF}$"
#     else:
#         if fluxmatch.offset:
#             _labelx1 = "(Sm - Sp)"
#             _labelx2 = "(Sm - Sp) - CF"
#         else:
#             _labelx1 = "Sm / Sp"
#             _labelx2 = "Sm / Sp - CF"


#     nan_color = "black"
#     cmap1 = plt.get_cmap(cmap1)
#     cmap2 = plt.get_cmap(cmap2)

#     cmap2 = make_divergent_cmap("Oranges_r", "Blues")

#     colors1 = [complement(cmap2(0.25))[:-1], complement(cmap2(0.75))[:-1]]
#     colors1 = ["black", "grey"]

#     plt.close("all")
#     font_labels = 18
#     font_ticks = 16

#     figsize = (10, 10)
#     fig = plt.figure(figsize=figsize)

#     gs = GridSpec(2, 2, wspace=0.08,
#                         hspace=0.22,
#                         left=0.02,
#                         right=1-0.07,
#                         top=1.-0.02,
#                         bottom=0.1)

#     wcs = WCS(header).celestial
#     axes = get_axes(4, gs, fig)

#     vmin1, vmax1 = fluxmatch.get_minmax("ratio")
#     # vmin1 = np.log10(vmin1)
#     # vmax2 = np.log10(vmax1)
#     vmin2, vmax2 = fluxmatch.get_minmax("residual")

#     norm1 = mpl.colors.Normalize(vmin=vmin1, vmax=vmax1)
#     norm1 = MidpointNormalize(vmin=vmin1, vmax=vmax1, midpoint=np.mean((fluxmatch.mdata.ratio)))
#     norm2 = MidpointNormalize(vmin=vmin2, vmax=vmax2, midpoint=0.)

#     # Axis 1: factor map with calibrator/test factors:
#     ax1 = plt.axes(axes[0], projection=wcs)
#     ax1.imshow(fluxmatch.factors, cmap=cmap1, norm=norm1, origin="lower",
#                aspect="auto")
#     ax1.scatter(fluxmatch.mdata.ra, fluxmatch.mdata.dec,
#                 c=fluxmatch.mdata.ratio, edgecolors=colors1[0], s=100,
#                 marker="o", norm=norm1, cmap=cmap1,
#                 transform=ax1.get_transform("world"))
#     if fluxmatch.tdata is not None:
#         ax1.scatter(fluxmatch.tdata.ra, fluxmatch.tdata.dec,
#                     c=fluxmatch.tdata.ratio, edgecolors=colors1[1], s=100,
#                     marker="s", norm=norm1, cmap=cmap1,
#                     transform=ax1.get_transform("world"))

#     for i in range(2):
#         ax1.coords[i].set_ticklabel_visible(False)

#     # Axis 3: residual map:
#     ax2 = plt.axes(axes[2], projection=wcs)
#     ax2.scatter(fluxmatch.mdata.ra, fluxmatch.mdata.dec,
#                 c=fluxmatch.mdata.residual, edgecolors=colors1[0], s=100,
#                 marker="o", norm=norm2, cmap=cmap2,
#                 transform=ax2.get_transform("world"))
#     if fluxmatch.tdata is not None:
#         ax2.scatter(fluxmatch.tdata.ra, fluxmatch.tdata.dec,
#                     c=fluxmatch.tdata.residual, edgecolors=colors1[1], s=100,
#                     marker="s", norm=norm2, cmap=cmap2,
#                     transform=ax2.get_transform("world"))

#     for i in range(2):
#         ax2.coords[i].set_ticklabel_visible(False)


#     # set up colorbar axes:
#     sb1 = SubplotSpec(gs, 0)
#     sp1 = sb1.get_position(fig).get_points().flatten()
#     cbax1 = [sp1[0], sp1[1]-0.005-0.015, sp1[2]-0.02, 0.015]
#     sb2 = SubplotSpec(gs, 2)
#     sp2 = sb2.get_position(fig).get_points().flatten()
#     cbax2 = [sp2[0], sp2[1]-0.005-0.015, sp2[2]-0.02, 0.015]


#     colorbar_axis1 = fig.add_axes(cbax1)
#     colorbar1 = mpl.colorbar.ColorbarBase(colorbar_axis1, cmap=cmap1,
#                                           norm=norm1,
#                                           orientation="horizontal")
#     colorbar1.set_label(_labelx1,
#                         fontsize=font_labels-2,
#                         labelpad=0.,
#                         verticalalignment="top",
#                         horizontalalignment="center")
#     colorbar1.ax.tick_params(which="major",
#                              labelsize=font_ticks-2,
#                              length=5.,
#                              color="black",
#                              labelcolor="black",
#                              width=1.)

#     colorbar_axis2 = fig.add_axes(cbax2)
#     colorbar2 = mpl.colorbar.ColorbarBase(colorbar_axis2, cmap=cmap2,
#                                           norm=norm2,
#                                           orientation="horizontal")
#     colorbar2.set_label(_labelx2,
#                         fontsize=font_labels-2,
#                         labelpad=0.,
#                         verticalalignment="top",
#                         horizontalalignment="center")
#     colorbar2.ax.tick_params(which="major",
#                              labelsize=font_ticks-2,
#                              length=5.,
#                              color="black",
#                              labelcolor="black",
#                              width=1.)


#     # histograms:
#     ax3 = plt.axes(axes[1])
#     ax4 = plt.axes(axes[3])

#     ax3.hist(np.log10(fluxmatch.mdata.ratio), bins=40, histtype="step",
#              color=colors1[0],
#              fill=False, label="Calibrators", density=True)
#     mu1, std1 = normal.fit(np.log10(fluxmatch.mdata.ratio))
#     x1 = np.linspace(ax3.get_xlim()[0], ax3.get_xlim()[1], 1000)
#     ax3.plot(x1, normal.pdf(x1, mu1, std1), color=colors1[0], ls="-")
#     print((mu1, std1))

#     ax3.hist(np.log10(fluxmatch.mdata.ratio/fluxmatch.mdata.model), bins=40, histtype="step",
#              color="crimson",
#              fill=False, label="Cal. model", density=True)
#     mu11, std11 = normal.fit(np.log10(fluxmatch.mdata.ratio/fluxmatch.mdata.model))
#     ax3.plot(x1, normal.pdf(x1, mu11, std11), color="crimson",
#              ls="-", )


#     if fluxmatch.tdata is not None:
#         ax3.hist(np.log10(fluxmatch.tdata.ratio), bins=40,
#                  histtype="step", color=colors1[1],
#                  fill=False, label="Test sources", density=True)
#         mu2, std2 = normal.fit(np.log10(fluxmatch.tdata.ratio))
#         ax3.plot(x1, normal.pdf(x1, mu2, std2), color=colors1[1], ls="-",
#                  )
#         print((mu2, std2))

#         ax3.hist(np.log10(fluxmatch.tdata.ratio/fluxmatch.tdata.model), bins=40, histtype="step",
#              color="dodgerblue",
#              fill=False, label="Test model", density=True)
#         mu11, std11 = normal.fit(np.log10(fluxmatch.tdata.ratio/fluxmatch.tdata.model))
#         ax3.plot(x1, normal.pdf(x1, mu11, std11), color="dodgerblue",
#                  ls="-", )

#     # ax3.set_xscale("log")

#     # ax4.hist(residuals, bins=40, histtype="step", color=colors1[0],
#              # fill=False, label="Calibrators", density=False)
#     # if test_sources:
#         # ax4.hist(tresiduals, bins=40, histtype="step", color=colors1[1],
#                  # fill=False, label="Test sources", density=False)


#     fig.savefig(outname)


# def fit_snr(ratios, snr):
#     """
#     """

#     try:
#         popt, pcov = curve_fit(logx, snr, ratios,
#                                p0=[0.01, -100., 1.],
#                                maxfev=10000)
#         perr = np.sqrt(np.diag(pcov))
#     except RuntimeError:
#         logger.warning("No logx function could be fit to SNR.")
#         popt = perr = np.full((3,), np.nan)

#     return popt, perr


# def snr_flux_plot(ratios, snr, outname, uselatex=True):
#     """
#     """

#     USE_LATEX, _ = set_font(uselatex)

#     plt.close("all")
#     size = (8, 6)
#     axes = [0.15, 0.1, 0.83, 0.88]

#     fig = plt.figure(figsize=size)
#     ax1 = plt.axes(axes)

#     if USE_LATEX:
#         ylabel = r"$S_\mathrm{measured} / S_\mathrm{predicted}$"
#     else:
#         ylabel = "S(measured) / S(predicted)"
#     xlabel = "SNR"

#     # popt, perr = fit_snr(ratios, snr)

#     ax1.plot(snr, ratios, ms=5., marker="o", c="crimson", ls="",
#              label="Data")
#     ax1.set_xscale("log")

#     # if np.isfinite(popt).all():
#     #     xmodel = np.linspace(min(snr), max(snr), 1000)
#     #     if USE_LATEX:
#     #         label = "$R = {:.3f}\\mathrm{{ln}}(\\mathrm{{SNR}}{:+.2f}){:+.2f}$".format(
#     #             popt[0], popt[1], popt[2])
#     #     else:
#     #         label = "R = {:.3f}ln(SNR{:+.2f}){:+.2f}".format(
#     #             popt[0], popt[1], popt[2])
#     #     ax1.plot(xmodel, logx(xmodel, *popt), ls="--", c="black", zorder=10,
#     #              label=label)
#     #     legend = ax1.legend(loc="best", shadow=False, fancybox=False,
#     #                          frameon=True, fontsize=16., numpoints=1)

#     ax1.tick_params(axis="both", which="both", labelsize=20.,
#                     pad=7.)
#     ax1.tick_params(which="major", length=10.)
#     ax1.tick_params(which="minor", length=5.)
#     ax1.set_xlabel(xlabel, fontsize=22., labelpad=0)
#     ax1.set_ylabel(ylabel, fontsize=22., labelpad=2.)

#     plt.savefig(outname, dpi=72)


# def writeout_results(ratios, ra, dec, snr, outname, delimiter=","):
#     """
#     """

#     with open(outname, "w+") as f:
#         f.write("#ra,dec,ratio,snr\n")

#         for i in range(len(ratios)):

#             f.write("{},{},{},{}\n".format(ra[i], dec[i], ratios[i], snr[i]))


# def image_plot(image, ratios, ra, dec, outname, cmap1="gray", cmap2="plasma",
#                offset=False, uselatex=True):
#     """
#     """

#     USE_LATEX, _ = set_font(uselatex)

#     font_ticks, font_labels = 20., 24.
#     size = (10, 10)
#     axes = [0.15, 0.1, 0.83, 0.8]
#     r = 8./10.
#     cbax = [0.15, axes[1]+axes[3]+0.01, axes[2], 0.03*0.8*r]


#     hdu = fits.open(image)[0]
#     wcs = WCS(hdu.header).celestial

#     plt.close("all")
#     fig = plt.figure(figsize=size)
#     ax1 = plt.axes(axes, projection=wcs)


#     vmin, vmax = auto_v(0.25, 99.5, np.squeeze(hdu.data))
#     ax1.imshow(np.squeeze(hdu.data),
#                vmin=vmin,
#                vmax=vmax,
#                cmap=cmap1,
#                origin="lower",
#                aspect="auto")  # fixed?

#     norm = mpl.colors.Normalize(vmin=min(ratios), vmax=max(ratios))
#     ax1.scatter(ra, dec, s=60, c=ratios, norm=norm, cmap=cmap2, marker="o",
#                 edgecolors="black", alpha=1.,
#                 transform=ax1.get_transform("world"))

#     ax1.grid(color="white", ls="--")

#     if USE_LATEX:
#         ticklabels = [(r"$^\mathrm{h}$", r"$^\mathrm{m}$", r"$^\mathrm{s}$"),
#                       (r"$^\mathrm{d}$", r"$^{\prime}$", r"$^{\prime\prime}$")]
#         if offset:
#             cbar_label = r"$S_\mathrm{measured} - S_\mathrm{predicted}$"
#         else:
#             cbar_label = r"$S_\mathrm{measured}/S_\mathrm{predicted}$"
#     else:
#         ticklabels = [("h", "m", "s"), ("d", "\'", "\"")]
#         if offset:
#             cbar_label = "S(measured) - S(predicted)"
#         else:
#             cbar_label = "S(measured) / S(predicted)"

#     ax1.coords[0].set_major_formatter("hh:mm")
#     ax1.coords[1].set_major_formatter("dd")
#     for i in range(2):
#         ax1.coords[i].set_ticks(color="white", size=8)
#         ax1.coords[i].display_minor_ticks(True)
#         ax1.coords[i].set_minor_frequency(5)
#         ax1.coords[i].set_ticklabel(size=font_ticks)
#         ax1.coords[i].set_separator(ticklabels[i])
#     ax1.coords[0].set_axislabel(r"R. A. (J2000)", size=font_labels)
#     ax1.coords[1].set_axislabel(r"Decl. (J2000)", size=font_labels)

#     colorbar_axis = fig.add_axes(cbax)
#     colorbar = mpl.colorbar.ColorbarBase(colorbar_axis,
#                                          cmap=cmap2,
#                                          norm=norm,
#                                          orientation="horizontal")

#     colorbar.set_label(cbar_label, fontsize=font_labels-2,
#                        verticalalignment="bottom",
#                        horizontalalignment="center", labelpad=10.)
#     colorbar.ax.tick_params(which="major", labelsize=font_ticks-2)

#     colorbar.ax.xaxis.set_ticks_position("top")
#     colorbar.ax.xaxis.set_label_position("top")

#     fig.savefig(outname, dpi=72)
#     plt.close("all")


# def plot_residuals(factors, header, ratios, pra, pdec, cmap="Spectral_r",
#                    tratios=None, tra=None, tdec=None, outname=None,
#                    model=None, use_midpoint_norm=False, offset=False,
#                    uselatex=True):
#     """
#     """

#     USE_LATEX, _ = set_font(uselatex)

#     nan_color = "black"
#     cmap = plt.get_cmap(cmap)

#     cmap = make_divergent_cmap("Oranges_r", "Blues")


#     colors = ["crimson", "dodgerblue", "black", "grey"]
#     colors = ["black", "grey"]
#     # colors = [complement(cmap(0.25))[:-1], complement(cmap(0.75))[:-1]]
#     cmap.set_bad(color=nan_color)
#     if cmap.name in ["Spectral", "Spectral_r"]:
#         comp_color = "black"
#     else:
#         comp_color = complement(cmap(11))[:-1]


#     plt.close("all")
#     font_labels = 18.
#     font_ticks = 16.
#     R = 1.

#     figsize = (12, 8)
#     fig = plt.figure(figsize=figsize)

#     gs = GridSpec(2, 3,
#                   wspace=0.08,
#                   hspace=0.22,
#                   left=0.02,
#                   right=1-0.07,
#                   top=1.-0.02,
#                   bottom=0.1)

#     wcs = WCS(header).celestial
#     ax1 = plt.subplot(gs[:, :-1], projection=wcs)
#     ax2 = plt.subplot(gs[0, -1])
#     ax3 = plt.subplot(gs[1, -1])

#     _labelhisty = "N"
#     if USE_LATEX:
#         if offset:
#             _labelx = r"$(S_\mathrm{measured} - S_\mathrm{predicted}) - \mathrm{CF}$"
#         else:
#             _labelx = r"$S_\mathrm{measured}/S_\mathrm{predicted} - \mathrm{CF}$"
#     else:
#         if offset:
#             _labelx = "(Sm - Sp) - CF"
#         else:
#             _labelx = "Sm/Sp - CF"


#     yi, xi = wcs.all_world2pix(pra, pdec, 0)
#     xi = xi.astype("i")
#     yi = yi.astype("i")
#     model_ratios = np.log10(factors[xi, yi].flatten())

#     res = np.log10(ratios) - model_ratios

#     if tratios is not None:
#         yi, xi = wcs.all_world2pix(tra, tdec, 0)
#         xi = xi.astype("i")
#         yi = yi.astype("i")
#         tmodel_ratios = np.log10(factors[xi, yi].flatten())
#         tres = np.log10(tratios) - tmodel_ratios
#         norm = MidpointNormalize(vmin=min([min(res), min(tres)]),
#                                  vmax=max([max(res), max(tres)]),
#                                  midpoint=0.)
#     else:
#         norm = MidpointNormalize(vmin=min(res), vmax=max(res),
#                                  midpoint=0.)


#     sb1 = SubplotSpec(gs, 0)
#     sp1 = sb1.get_position(fig).get_points().flatten()
#     x = sp1[0]
#     gp1 = ax1.get_position().get_points().flatten()
#     dx = gp1[2] - gp1[0]
#     sb2 = SubplotSpec(gs, 4)
#     sp2 = sb2.get_position(fig).get_points().flatten()
#     y = sp2[1]
#     cbax = [x, y-0.025, dx, 0.02]

#     ax1.scatter(pra, pdec, c=res, edgecolors=colors[0], s=100, marker="o",
#                 norm=norm, cmap=cmap, transform=ax1.get_transform("world"),
#                 label="Calibrators")

#     if tratios is not None:
#         ax1.scatter(tra, tdec, c=tres, edgecolors=colors[1], s=100, marker="s",
#             norm=norm, cmap=cmap, transform=ax1.get_transform("world"),
#             label="Test sources")

#     legend1 = ax1.legend(loc="best", shadow=False, fancybox=False,
#                              frameon=True, fontsize=16., numpoints=1)
#     legend1.get_frame().set_edgecolor("dimgrey")

#     # ax1.set_xlim([min(xi), max(xi)])
#     # ax1.set_ylim([min(yi), max(yi)])

#     for i in range(2):
#         ax1.coords[i].set_ticklabel_visible(False)
#         ax1.coords[i].grid(color="black", linestyle="--")

#     colorbar_axis = fig.add_axes(cbax)
#     colorbar = mpl.colorbar.ColorbarBase(colorbar_axis, cmap=cmap, norm=norm,
#                                          orientation="horizontal")
#     colorbar.set_label(_labelx,
#                        fontsize=font_labels,
#                        labelpad=0.,
#                        verticalalignment="top",
#                        horizontalalignment="center")
#     colorbar.ax.tick_params(which="major",
#                             labelsize=font_ticks,
#                             length=5.,
#                             color="black",
#                             labelcolor="black",
#                             width=1.)


#     # set up some histograms
#     ax2.hist(res, bins=40, histtype="step", color=colors[0],
#              fill=False)

#     # Do some Gaussian fitting to the residuals:
#     params, err_params = determine_fluxscale_err(res)
#     if np.isnan(params[2]):
#         logger.warning("residuals fitting failed!")
#         params[:] = 0.
#         err_params[:] = 0.
#     else:
#         if USE_LATEX:
#             gauss_label = "$\\mu={:.3f}\\pm{:.3f}$\n".format(params[1], err_params[1]) + \
#                           "$\\sigma={:.2f}\\pm{:.2f}$".format(params[2], err_params[2])
#         else:
#             gauss_label = "mu = {:.3f} +/- {:.3f}\n".format(params[1], err_params[1]) + \
#                           "sigma = {:.2f} +/ {:.2f}".format(params[2], err_params[2])
#         gauss_model = np.linspace(np.nanmin(res), np.nanmax(res), 1000)
#         ax2.plot(gauss_model, gaussian(gauss_model, *params), ls="-",
#                  c=colors[0], label=gauss_label)
#         legend2 = ax2.legend(loc="best", shadow=False, fancybox=False,
#                              frameon=True, fontsize=16., numpoints=1)
#         legend2.get_frame().set_edgecolor("dimgrey")


#     ax2.set_xlabel(_labelx, fontsize=font_labels)
#     ax2.tick_params(axis="both", which="both", labelsize=14.)
#     ax2.yaxis.tick_right()


#     if tratios is not None:
#         ax3.hist(tres, bins=40, histtype="step", color=colors[1],
#                  fill=False)

#         # Do some Gaussian fitting to the residuals:
#         params, err_params = determine_fluxscale_err(tres)
#         if np.isnan(params[2]):
#             logger.warning("residuals fitting failed!")
#             params[:] = 0.
#             err_params[:] = 0.
#         else:
#             if USE_LATEX:
#                 gauss_label = "$\\mu={:.3f}\\pm{:.3f}$\n".format(params[1], err_params[1]) + \
#                               "$\\sigma={:.2f}\\pm{:.2f}$".format(params[2], err_params[2])
#             else:
#                 gauss_label = "mu = {:.3f} +/- {:.3f}\n".format(params[1], err_params[1]) + \
#                               "sigma = {:.2f} +/ {:.2f}".format(params[2], err_params[2])
#             gauss_model = np.linspace(np.nanmin(tres), np.nanmax(tres), 1000)
#             ax3.plot(gauss_model, gaussian(gauss_model, *params), ls="-",
#                      c=colors[1], label=gauss_label)
#             legend3 = ax3.legend(loc="best", shadow=False, fancybox=False,
#                                  frameon=True, fontsize=16., numpoints=1)
#             legend3.get_frame().set_edgecolor("dimgrey")

#         ax3.set_xlabel(_labelx, fontsize=font_labels)
#         ax3.tick_params(axis="both", which="both", labelsize=14.)
#         ax3.yaxis.tick_right()


#     ax2.set_xlabel(_labelx, fontsize=font_labels)
#     ax2.tick_params(axis="both", which="both", labelsize=14.)
#     ax2.yaxis.tick_right()


#     fig.savefig(outname, dpi=72)


# def residual_plot(ratios, flux, pra, pdec, outname, factors, header,
#                   tratios=None, tra=None, tdec=None, tflux=None,
#                   cmap="plasma", uselatex=True, offset=False):
#     """Make a plot of the residuals from the model ratios.

#     """

#     USE_LATEX, _ = set_font(uselatex)
#     font_ticks, font_labels = 20., 24.
#     size = (10, 8)
#     axes = [0.15, 0.25, 0.83, 0.88-0.145]
#     axesr = [0.15, 0.1, 0.83, 0.145]
#     axes[2] -= 0.3
#     axesr[2] -= 0.3
#     axesh = [axes[0]+axes[2]+0.02, axes[1], 0.3-0.02, axes[3]]

#     fig = plt.figure(figsize=size)
#     ax1 = plt.axes(axes)
#     axr = plt.axes(axesr)
#     axh = plt.axes(axesh)


#     # Need to find the values in the data array:
#     wcs = WCS(header).celestial
#     y, x = wcs.all_world2pix(pra, pdec, 0)
#     x = x.astype("i")
#     y = y.astype("i")
#     model_ratios = factors[x, y].flatten()

#     # plot ratios,
#     ax1.plot(flux, ratios, ls="", marker="o", c="crimson", zorder=2, label="Data")
#     # plot model ratios at source position,
#     ax1.plot(flux, model_ratios, ls="", marker="s", c="dodgerblue", zorder=1,
#              label="Model")

#     ax1.set_xscale("log")
#     if offset:
#         yval = ratios-model_ratios
#     else:
#         yval = 1. - (ratios/model_ratios)
#     axr.plot(flux, yval, ls="", marker="o", c="crimson", zorder=2)
#     axr.axhline(0., ls="-", color="black", zorder=10)
#     axr.set_xscale("log")

#     hist_vals = [ratios, model_ratios]
#     hist_colors = ["crimson", "dodgerblue", "black", "grey"]
#     hist_linestyles = ["-", "--", "-.", ":"]

#     if np.asarray([opt is not None for opt in [tratios, tra, tdec, tflux]]).all():


#         logger.debug("using test sources")
#         y, x = wcs.all_world2pix(tra, tdec, 0)
#         x = x.astype("i")
#         y = y.astype("i")
#         tmodel_ratios = factors[x, y].flatten()

#         ax1.plot(tflux, tratios, ls="", marker="v", c="black", zorder=3, label="Test",
#                  )
#         ax1.plot(tflux, tmodel_ratios, ls="", marker="^", c="grey", zorder=2, label="Test model")
#         if offset:
#             yval = tratios-tmodel_ratios
#         else:
#             yval = 1. - (tratios/tmodel_ratios)
#         axr.plot(tflux, yval, ls="", marker="v", c="black", zorder=3,)

#         hist_vals.append(tratios)
#         hist_vals.append(tmodel_ratios)

#     mean_, median_ = np.mean(ratios), np.median(ratios)

#     ax1.axhline(mean_, ls="-", color="black", zorder=10, label="Mean")
#     ax1.axhline(median_, ls="--", color="black", zorder=10, label="Median")

#     for i in range(len(hist_vals)):
#         axh.hist(hist_vals[i], bins=len(ratios)//10, range=ax1.get_ylim(),
#                  histtype="step", color=hist_colors[i], fill=False,
#                  orientation="horizontal", density=True, ls=hist_linestyles[i],
#                  lw=2.)

#     legend = ax1.legend(loc="best", shadow=False, fancybox=False, frameon=True,
#                         fontsize=font_ticks-4, numpoints=1)
#     legend.get_frame().set_edgecolor("dimgrey")

#     labelx = "Flux density"
#     if USE_LATEX:
#         if offset:
#             labely = r"$S_\mathrm{measured} - S_\mathrm{predicted}$"
#             labelr = r"$\Delta S - \mathrm{model}$"
#         else:
#             labely = r"$S_\mathrm{measured}/S_\mathrm{predicted}$"
#             labelr = r"$S_\mathrm{m}/S_\mathrm{p} - \mathrm{model}$"
#     else:
#         if offset:
#             labely = "S(measured) - S(predicted)"
#             labelr = "(Sm - Sp) - model"
#         else:
#             labely = "S(measured) / S(predicted)"
#             labelr = "(Sm / Sp) - model"

#     ax1.set_ylabel(labely, fontsize=font_labels)
#     axr.set_ylabel(labelr, fontsize=font_ticks)
#     axr.set_xlabel(labelx, fontsize=font_labels)
#     # axh.set_xlabel("N", fontsize=font_labels)

#     ax1.tick_params(labelsize=font_ticks, length=5.0)
#     axh.tick_params(labelsize=font_ticks, length=5.0, axis="x")
#     ax1.xaxis.set_ticklabels([])
#     axh.yaxis.set_ticklabels([])
#     axr.tick_params(labelsize=font_ticks, length=5.0, axis="x", pad=5)
#     axr.tick_params(labelsize=font_ticks-2, length=5.0, axis="y")

#     fig.savefig(outname, dpi=72)
#     plt.close("all")

